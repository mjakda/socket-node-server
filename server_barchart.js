function BarChartObj() {

};

BarChartObj.prototype.makeData = function () {
    var self = this;
    console.warn('we r in make Data');

    var inData = [];
    for(var i = 0; i < 5; i++) {
        inData.push({"number":i, "value": Math.floor(Math.random()*64)});
    }
    process.send({ fakeData: inData });

    // var setup to exit loop in "stop" function
    this._state = setTimeout(function () {
        self.makeData();
    }, 3000);

};

BarChartObj.prototype.randomInt = function (min,max) {
    return Math.floor(Math.random()*(max-min+1)+min * 2);
}



BarChartObj.prototype.sendGridData = function () {
    var self = this;
    console.warn('we r in make Grid Data');

    var indata = [{"status":"Cancelled","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"VIL011","advertiser":"Villavida Properties S.L.","qty":self.randomInt(1000, 2000)},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Sep-12","date":"01/09/2012","code":"IMM256","advertiser":"IMMOBILIARE SOFIA S.R.L","qty":11500},
        {"status":"Live","title":"EasyJet TAD","issue":"May-13","date":"01/05/2013","code":"ATN001","advertiser":"At","qty":4426},
        {"status":"Live","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"LEO002","advertiser":"Leo Services B.V","qty":110000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes March 2013","date":"01/03/2013","code":"STA010","advertiser":"Starka Resturant","qty":0},
        {"status":"Live","title":"EasyJet TAD","issue":"Jul-13","date":"01/07/2013","code":"ATN001","advertiser":"At","qty":4426},
        {"status":"Cancelled","title":"EasyJet TAD","issue":"Mar-13","date":"01/03/2013","code":"LAW046","advertiser":"Lawrence","qty":2000},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Feb-13","date":"01/02/2013","code":"TAU001","advertiser":"Taurus Catering LTD","qty":20000},
        {"status":"Live","title":"EasyJet TAD","issue":"Jun-13","date":"01/06/2013","code":"ATN001","advertiser":"At","qty":4426},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Feb-13","date":"01/02/2013","code":"PMS001","advertiser":"PM 224 srl","qty":50000},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"GOZ001","advertiser":"Gozo Pride Tours","qty":5000},
        {"status":"Live","title":"EasyJet TAD","issue":"Apr-13","date":"01/04/2013","code":"ATN001","advertiser":"At","qty":4426},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Feb-13","date":"01/02/2013","code":"JOC001","advertiser":"Jockey Club","qty":4000},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"ALM281","advertiser":"Al Mourjan","qty":0},
        {"status":"Live","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"TIV007","advertiser":"Tivoli A/S","qty":10000},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Mar-14","date":"01/03/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"KLM TAD","issue":"May-14","date":"01/05/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Feb-14","date":"01/02/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes December 2013","date":"01/12/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Jul-13","date":"01/07/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-14","date":"01/03/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Aug-13","date":"01/08/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Apr-14","date":"01/04/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Nov-13","date":"01/11/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"KLM TAD","issue":"Nov-13","date":"01/11/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes August 2013","date":"01/08/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes September 2013","date":"01/09/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Apr-14","date":"01/04/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Jun-14","date":"01/06/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"KLM TAD","issue":"Aug-13","date":"01/08/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"May-14","date":"01/05/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"KLM TAD","issue":"Jun-14","date":"01/06/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Oct-13","date":"01/10/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes January 2014","date":"01/01/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Jan-14","date":"01/01/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes November 2013","date":"01/11/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Oct-13","date":"01/10/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Jan-14","date":"01/01/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Sep-13","date":"01/09/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes October 2013","date":"01/10/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Jul-13","date":"01/07/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"KLM TAD","issue":"Sep-13","date":"01/09/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Dec-13","date":"01/12/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":15000},
        {"status":"Cancelled","title":"KLM TAD","issue":"Feb-14","date":"01/02/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Dec-13","date":"01/12/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes June 2014","date":"01/06/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"BRA443","advertiser":"Bradran Persian Store","qty":0},
        {"status":"Live","title":"EasyJet TAD","issue":"Aug-13","date":"01/08/2013","code":"VEL074","advertiser":"Vela Spa","qty":20000},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Feb-13","date":"01/02/2013","code":"ARQ013","advertiser":"Arquivo de Lembran","qty":2500},
        {"status":"Live","title":"EasyJet TAD","issue":"Jul-13","date":"01/07/2013","code":"VEL074","advertiser":"Vela Spa","qty":20000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes February 2014","date":"01/02/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Live","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"STE006","advertiser":"Stedman Communications LTD","qty":10000},
        {"status":"Cancelled","title":"EasyJet TAD","issue":"Feb-13","date":"01/02/2013","code":"MER007","advertiser":"The Merchant City Inn","qty":12000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes July 2013","date":"01/07/2013","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Apr-12","date":"01/04/2012","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Live","title":"EasyJet TAD","issue":"Apr-13","date":"01/04/2013","code":"HAV004","advertiser":"Ibis Hotels","qty":20000},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Apr-13","date":"01/04/2013","code":"APO001","advertiser":"ITS Ltd","qty":100000},
        {"status":"Live","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"APO001","advertiser":"ITS Ltd","qty":100000},
        {"status":"Live","title":"KLM TAD","issue":"Apr-13","date":"01/04/2013","code":"VIV001","advertiser":"Vivanta by Taj  Gurgaon","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"BOO005","advertiser":"BOOTES PLUS SL","qty":0},
        {"status":"Live","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Live","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"OPP016","advertiser":"Opportunity Business SL","qty":30000},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Feb-13","date":"01/02/2013","code":"CSL002","advertiser":"CSL (CAR STORAGE LTD)","qty":15000},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"MAL007","advertiser":"The Malta Experience","qty":2500},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Mar-13","date":"01/03/2013","code":"GES002","advertiser":"Gesalf SS","qty":1500},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Feb-13","date":"01/02/2013","code":"SER008","advertiser":"Serviced Apartment Amsterdam","qty":22000},
        {"status":"Live","title":"Germanwings TAD","issue":"Mar-13","date":"01/03/2013","code":"TEC144","advertiser":"Technische Universit","qty":0},
        {"status":"Live","title":"Germanwings TAD","issue":"May-13","date":"01/05/2013","code":"TEC144","advertiser":"Technische Universit","qty":0},
        {"status":"Live","title":"EasyJet TAD","issue":"Jun-13","date":"01/06/2013","code":"VEL074","advertiser":"Vela Spa","qty":20000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes March 2014","date":"01/03/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Ryanair TAD","issue":"Feb-13","date":"01/02/2013","code":"PHE001","advertiser":"PHEBE ENTERPRISES Ltd","qty":12000},
        {"status":"Live","title":"EasyJet TAD","issue":"Apr-13","date":"01/04/2013","code":"HAV004","advertiser":"Ibis Hotels","qty":20000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes May 2014","date":"01/05/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Mar-13","date":"01/03/2013","code":"SER008","advertiser":"Serviced Apartment Amsterdam","qty":22000},
        {"status":"Live","title":"EasyJet TAD","issue":"Apr-13","date":"01/04/2013","code":"HAV004","advertiser":"Ibis Hotels","qty":20000},
        {"status":"Live","title":"EasyJet TAD","issue":"Apr-13","date":"01/04/2013","code":"HAV004","advertiser":"Ibis Hotels","qty":20000},
        {"status":"Cancelled","title":"Brussels Airlines TAD","issue":"Passes April 2014","date":"01/04/2014","code":"WIF001","advertiser":"WIFI METROPOLIS LLC","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"May-13","date":"01/05/2013","code":"URB092","advertiser":"Urbango","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Apr-13","date":"01/04/2013","code":"URB092","advertiser":"Urbango","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"May-13","date":"01/05/2013","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Jun-13","date":"01/06/2013","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Feb-14","date":"01/02/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Sep-14","date":"01/09/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Jan-14","date":"01/01/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Oct-13","date":"01/10/2013","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-14","date":"01/03/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Jun-13","date":"01/06/2013","code":"URB092","advertiser":"Urbango","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"SER204","advertiser":"Seripacific Hotel","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Aug-14","date":"01/08/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Apr-14","date":"01/04/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Dec-13","date":"01/12/2013","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Aug-13","date":"01/08/2013","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"Carlson Wagonlit TAD","issue":"Feb-13","date":"01/02/2013","code":"DIA002","advertiser":"Diamond Bank Plc","qty":90000},
        {"status":"Live","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"VIV001","advertiser":"Vivanta by Taj  Gurgaon","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"DIA002","advertiser":"Diamond Bank Plc","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Jun-14","date":"01/06/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Oct-14","date":"01/10/2014","code":"BAI039","advertiser":"Baia Seafood Restaurant","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Feb-13","date":"01/02/2013","code":"VIV001","advertiser":"Vivanta by Taj  Gurgaon","qty":0},
        {"status":"Cancelled","title":"KLM TAD","issue":"Mar-13","date":"01/03/2013","code":"URB092","advertiser":"Urbango","qty":0}];

    process.send({ gridData:indata });

    // var setup to exit loop in "stop" function
    this._state = setTimeout(function () {
        self.sendGridData();
    }, 3000);

};
BarChartObj.prototype.sendInventoryData = function () {
    var self = this;
    console.warn('we r in make Stacked  Data');

    var inData =
        [
            [
                {
                    "time": "2014-02-17",
                    "y":  self.randomInt(10, 100)
                },
                {
                    "time": "2014-02-18",
                    "y": self.randomInt(10, 100)
                },
                {
                    "time": "2014-02-19",
                    "y": self.randomInt(100, 200)
                },
                {
                    "time": "2014-02-20",
                    "y": self.randomInt(50, 80)
                },
                {
                    "time": "2014-02-21",
                    "y": self.randomInt(50, 80)
                }
            ],
            [
                {
                    "time": "2014-02-17",
                    "y": self.randomInt(10, 50)
                },
                {
                    "time": "2014-02-18",
                    "y": self.randomInt(10, 50)
                },
                {
                    "time": "2014-02-19",
                    "y": self.randomInt(100, 200)
                },
                {
                    "time": "2014-02-20",
                    "y": self.randomInt(10, 150)
                },
                {
                    "time": "2014-02-21",
                    "y": self.randomInt(10, 250)
                }
            ],
            [
                {
                    "time": "2014-02-17",
                    "y": self.randomInt(1, 80)
                },
                {
                    "time": "2014-02-18",
                    "y": self.randomInt(1, 80)
                },
                {
                    "time": "2014-02-19",
                    "y": self.randomInt(10, 200)
                },
                {
                    "time": "2014-02-20",
                    "y": self.randomInt(40, 150)
                },
                {
                    "time": "2014-02-21",
                    "y": self.randomInt(1, 40)
                }
            ]
        ];

//        [[{"day":"Mon","Sold":339,"Reserved":345,"Unsold":186},
//        {"day":"Tues","Sold":197,"Reserved":272,"Unsold":10},
//        {"day":"Wed","Sold":163,"Reserved":207,"Unsold":140},
//        {"day":"Thurs","Sold":120,"Reserved":166,"Unsold":128},
//        {"day":"Fri","Sold":320,"Reserved":203,"Unsold":123}]];



//    var inData = { days:[{day:"Mon",Sold:339,Reserved:345,Unsold:186},
//            {day:"Tues",Sold:197,Reserved:272,Unsold:10},
//            {day:"Wed",Sold:163,Reserved:207,Unsold:140},
//            {day:"Thurs",Sold:120,Reserved:166,Unsold:128},
//            {day:"Fri",Sold:320,Reserved:203,Unsold:123}] };
    process.send({ InventoryData:inData });


    // var setup to exit loop in "stop" function
    this._state = setTimeout(function () {
        self.sendInventoryData();
    }, 3000);

};




BarChartObj.prototype.stop = function () {
    clearTimeout(this._state);
    this._state = null;
};

var barChartOpj = new BarChartObj();

//**** ENTRY point of script ****//



process.on('message', function (msg) {

    console.log(msg.op);
    if (msg.op === 'start') {
        //BarChartObj.execute();
    }
    else if (msg.op === 'getData') {
        barChartOpj.makeData();
    }
    else if (msg.op === 'stop') {
        barChartOpj.stop();
    }
    else if (msg.op === 'startStackedChart') {
        barChartOpj.sendInventoryData();
    }
    else if (msg.op === 'startGridData') {
        barChartOpj.sendGridData();
    }


    else {
     console.warn(msg + 'MESSAGE from server');
    }
});