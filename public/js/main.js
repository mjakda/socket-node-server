var app = angular.module('barchartApp', []);

app.controller('updateController', function ($scope, socket) {
    $scope.status = 'AngularJS successfully loaded.';

    $scope.fakeData = [{"number":0,"value":0},{"number":1,"value":0},{"number":2,"value":0},{"number":3,"value":0},{"number":4,"value":0}
    ];
    $scope.counter = 0;
    $scope.data = [];



    $scope.start = function () {
        socket.emit('start');
    };

    //onload send msg to server 'start'
    $scope.testData = function () {
        socket.emit('getStarted');
    };


    socket.on('status', function (data) {
        $scope.status = data.message;

        $scope.makeChart($scope.fakeData);
    });

    // on receiving data from server called 'fakeData' set ng var to value
    socket.on('fakeData', function (data) {
        $scope.fakeData = data.message;
    });

    /****** THIS method UPDATES the NG model data value ******/
    socket.on('update', function (data) {


        console.log('fakeData at beginning = ' +  $scope.fakeData.length);
        $scope.fakeData = data.fakeData; //displayed on data
        console.log('fakeData after data received = ' +  $scope.fakeData.length);

        $scope.makeChart($scope.fakeData);
    });

    /******** END **********/


    // Make bar chart
    $scope.makeChart = function(dataReceived){

       console.log('realtime barchart!');

       console.log( $scope.fakeData.length);

        var margin = {top: 20, right: 20, bottom: 30, left: 80},
            w = 515 - margin.left - margin.right,
            h = 260 - margin.top - margin.bottom;




        //simulate different data set NOT USED
//        $scope.updateData = function(){
//            var inData = [];
//            for(var i = 0; i < 5; i++) {
//                inData.push({"number":i, "value": Math.floor(Math.random()*64)});
//            }
//            return inData;
//        };

        $scope.data = dataReceived;
       // $scope.data = data;

        //console.log($scope.data);

        // map domain to chart range
        var x = d3.scale.linear()
            .domain([0, d3.max($scope.data, function(d) { return d.value; })])
            .range([0, w]); // horizontal bars

        var y = d3.scale.ordinal()
            .domain(d3.range($scope.data.length))
            .rangeBands([0, h], 0.1); // vertical of bars on top of each given padding

        // Y axis
        //define the look of the axis
        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(4); // how many ticks to show

        // X axis
        //define the look of the axis
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(10); // how many ticks to show

        // colour for bars
        var color = d3.scale.ordinal()
            .range(["#9400d3", "#ff6347"]);

        if( $scope.counter === 0 ) {
            $scope.counter = $scope.counter + 1;

            // render chart space
            $scope.svg = d3.select(".realtime")
                .append("svg")
                .attr("width", w + margin.left + margin.right)
                .attr("height", h + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



            //Add X axis
            $scope.svg.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(0," + (h) + ")")
                .call(xAxis);

            $scope.svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(-10,0)")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", -60)
                .attr("dy", "24")
                .attr("color", 'blue')
                .style("text-anchor", "end")
                .text("Number");



            // create main bar
            var bars = $scope.svg.selectAll(".bar")
                .data($scope.data)
                .enter().append("g")
                .attr("class", "bar")
                .attr("transform", function (d, i) {
                    return "translate(" + 0 + "," + y(i + 1) + ")";
                });

            //create the rectangles in these bars
            bars.append("rect")
                .attr("fill", function (d, i) {
                    return color(i % 2);
                })
                .attr("width", function (d) {
                    return x(d.value);
                })
                .attr("height", y.rangeBand()); //work out the padding and height of bars

        }
        else {
            var barData = [];

            barData = $scope.data;

            var rect = $scope.svg.selectAll(".bar rect").data(barData);
            var text = $scope.svg.selectAll(".bar text").data(barData);

            var delay = function (d, i) {
                return i * 50;
            };

            rect.transition().duration(750)
                .delay(delay)
                .attr("width", function (d) {
                    return x(d.value);
                });
            text.transition().duration(750)
                .delay(delay)
                .attr("x", function (d) {
                    return x(d.value);
                })

        }

    };
        
});

app.factory('socket', function ($rootScope) {
    var socket = io.connect();
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
});
