var jade = require('jade'),
	express = require('express'),
	app = express(),
    server = require('http').createServer(app),
	io = require('socket.io').listen(server);

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(express.logger());
app.use(express.compress());
app.use(express.static(__dirname + '/public'));
	
app.get('/', function(req, res){
	res.render('index');
});

server.listen(process.env.PORT || 4561);
console.log('listening on' + process.env.PORT || 4561);

// sub JS 'server_barchart' file on server for express socket IO to use - for non blocking functionality

var cp = require('child_process').fork('server_barchart');
cp.on('message', function (message) {
    //console.log(message);
    if(message.InventoryData){
        io.sockets.emit('updateForStackedchart', message);
    }
    else if(message.gridData){
        io.sockets.emit('updateForDatagrid', message);
    }
    else{
        io.sockets.emit('updateForBarchart', message);
    }


});

io.sockets.on('connection', function (socket) {
    socket.emit('status', { message: "Connection has been made" });
    // on successful connection send data to client
    socket.emit('testData', { message: "data initialised and sent to client" });
//
//
//
//
//
//    socket.on('getStarted', function (data) {
//        cp.send({ op: 'getData' }); // sends value/pair to child process to do some thing
//
//    });
   // cp.send({ op: 'getData' });
    //cp.send({ op: 'startStackedChart' });


// THIS BIT not working *************************

    // Success!  Now listen to messages to be received
    socket.on('startSocketA',function(data){
        console.log('Received message from client!',data);
        cp.send({ op: 'stop' });
        cp.send({ op: 'getData' });

    });

    socket.on('startSocketB', function (data) {
        console.log('Received message from client!',data);
        cp.send({ op: 'stop' });
        cp.send({ op: 'startStackedChart' });
    });

    socket.on('startSocketC', function (data) {

        console.log('Received message from client!',data);
        cp.send({ op: 'stop' });
        cp.send({ op: 'startGridData' });
    });



    socket.on('messages', function(data) {
        //console.log('The data received is -- ' + data);
    });

//    socket.on('stop', function (data) {
//        cp.send({ op: 'stop' });
//        socket.emit('status', { message: "Stopped" });
//    });




});
